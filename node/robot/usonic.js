#!/usr/bin/env node
'use strict'

/**
  *   ultrasonic range finder
  */

// stuff we need
const config = require( '../config.js' )

var median = ( data ) => {
  data = data.sort( ( a, b ) => {
    return a - b
  })
  var middle = ( data.length / 2 ) | 0
  if ( data.length % 2 ) {
    return data[middle]
  }
  return ( data[middle - 1] + data[middle] ) / 2
}

var toCM = ( time ) => {
  /* machOne + ( 0.6 * temp ) */
  var sndSpeed = 33130
  var ambiTemp = 22

  var newSpeed = sndSpeed + ( 0.6 * ambiTemp )
  var calc = ( time * newSpeed ) / 2

  return calc
}

var ultraSonic = ( trigger, echo ) => {

  var timeStart
  var timeStop

  wire.write( trigger, true )
  // sleep
  wire.write( trigger, false )
  timeStart = Date()

  var echoIn = wire.read( echo )

  while ( echoIn == 0 ) {
    timeStart = Date()
  }

  while ( echoIn == 1 ) {
    timeStop = Date()
  }

  var distance = toCM( timeStop - timeStart )

  // return () => {
  return distance
  // }
}

/* prince distace to screen */
var print = ( raw ) => {
  var cm = median( raw )
  process.stdout.clearLine()
  process.stdout.cursorTo(0)
  if ( cm < 0 ) {
    process.stdout.write( 'Timeout error!\n' )
  } else {
    process.stdout.write( 'Distance: ' + cm.toFixed(2) + ' cm.' )
  }
}

// module.exports.init = ( conf ) => {
var init = ( conf ) => {
  var sensor = ultraSonic( conf.trig, conf.echo )
  var distance = []
  

  ( function measure() {
    if ( !distance || distance.length === 5 ) {
      if ( distance ) {
        print( distance )
      }
      distance = []
    }
    setTimeout( () => {
      distance.push( sensor() )
      measure()
    }, 60 );
  }())

}

init({
  trig: 1,
  echo: 2
})

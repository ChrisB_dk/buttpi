$(document).ready( function () {

	const ip = location.host;
	const socket = io.connect( ip )
	var lastEvent = stop
	const url = 'http://192.168.42.137:8080/stream/video.mjpeg'

	$('#serverinfo').prepend( 'IP: ', ip, '\n' )
	$('#serverinfo').prepend( 'URL: ', url, '\n' )

	$('#stream').attr( 'src', url )

	$('[data-toggle="offcanvas"]').click( function () {
    $('.row-offcanvas').toggleClass('active')
  })

	socket.on( 'status', () => {
		var data = 1
		socket.emit( 'status', data )
	  $('#serverinfo').prepend( 'Status: ', data, '\n' )
	})

	function robot( control, value ) {
		if ( lastEvent !== value ) {
			$('#serverinfo').prepend( control, ' : ', value, '\n' )
			// console.log( control + ' : ' + value )
			socket.emit( control, value )
		}
		lastEvent = value
	}

	$('#speed').on( 'change input', function () {
		var value = $(this).val()
		robot( 'speed', value )
	})

	$(':button').mouseup( function () {
		robot( 'event', 'stop' )
	})

	$('#forward').mousedown( function () {
		robot( 'event', 'forward' )
	})

	$('#backward').mousedown( function () {
		robot( 'event', 'backward' )
	})

	$('#stop').mousedown( function () {
		robot( 'event', 'stop' )
	})

	$('#left').mousedown( function () {
		robot( 'event', 'left' )
	})

	$('#right').mousedown( function () {
		robot( 'event', 'right' )
	})

})

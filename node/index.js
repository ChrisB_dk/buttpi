#!/usr/bin/env node
'use strict'

/**
  * Robot script
  *
  *   from the desktop, bed and / or "not in the face"
  *   Bjørn 'Im still not dancing jive' Christiansen : ChrisB.dk'
  */

// get stuff we need //
const serv = require( './lib/serving' )
const web = require( './lib/sockets' )

serv.html()
web.init( serv.serve )


// painless quit, for now //
process.on( 'SIGINT', () => {
  process.stdout.write( 'Got SIGINT. Press Control-D to exit.\n' )
  process.exit(0)
})
process.on( 'exit', () => {
    process.stdout.write( 'THE END\n' )
})

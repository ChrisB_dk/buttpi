#!/usr/bin/env node
'use strict'

/**
	*	config stuff
	*/

/*  */
module.exports = {
	server: {
		'host': process.env.HOST || '0.0.0.0',
		'port': process.env.PORT || 8080
	},
	html: {
		'title': 'rPIbot remote control',
		'body': 'site.html'
	},
	mcp: {
		'pinBase': 100,
		'spiPort': 0,
		'devId': 0
	},
	usonic: {
		'trig': {
			'pin': 1,
			'dir': 'output',
			'type': 'digital'
		},
		'echo': {
			'pin': 2,
			'dir': 'input',
			'type': 'digital'
		},
		'machOne': 33130
	}
}

#!/usr/bin/env node
'use strict'

/**
  * a dunp to console function
  */

var toScreen = ( msg ) => {
  process.stdout.clearLine()
  process.stdout.cursorTo(0)
  process.stdout.write( msg )
}

#!/usr/bin/env node
'use strict'

/**
  *   http server
  *
  */

// hent noget vi har brug for
const http = require( 'http' )
const server = http.createServer()
const fs = require( 'fs' )
const path = require( 'path' )
const page = require( './html' )
const config = require( '../config' )

// MIME typer
const contentType = ( ext ) => {
	var type
	switch ( ext ) {
		case '.html':
			type = 'text/html'
			break
		case '.ico':
			type = 'image/x-icon'
			break
		case '.css':
			type = 'text/css'
			break
		case '.js':
			type = 'text/javascript'
			break
		case '.jpg':
			type = 'image/jpeg'
			break
		case '.jpeg':
			type = 'image/jpeg'
			break
		case '.png':
			type = 'image/png'
			break
		case '.svg':
			type = 'text/xml'
			break
		default:
			type = 'text/plain'
			break
	}
	return { 'Content-Type' : type }
}


// the http server function call
module.exports = {
	html: () => {
  	const port = process.env.PORT || config.server.port
		const host = process.env.HOST || config.server.host

  	server.on( 'request', ( request, response ) => {
			var filePath = './assets/'
			if ( request.url === '/' ) {
				response.writeHead( 200, contentType( '.html' ) )
				response.end( page.render() )
			} else {
				var type = path.parse( request.url )
				if ( type.name != 'main' && type.name != 'favicon' ) {
					filePath += 'vendor' + type.dir + '/'
					filePath += type.base
				} else {
					filePath += type.base
				}
				fs.readFile( filePath, ( error, data ) => {
					response.writeHead( 200,  contentType( type.ext ) )
					response.end( data )
				})
			}
		})
		server.listen( port, host, () => {
			console.log( 'server running on %s : %d', host, port )
		})
	}
}

module.exports.serve = server

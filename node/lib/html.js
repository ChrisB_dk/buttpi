#!/usr/bin/env node
'use strict'

/**
	* html code
	*/

// get stuff //
const fs = require( 'fs' )
const config = require( '../config' )

// some vars //
var page = {}

// RAW HTML5 //
page.start = [
	'<!DOCTYPE html>',
	'<html lang="da-DK">',
	'  <meta charset="utf-8" />',
	'  <meta http-equiv="x-ua-compatible" content="ie=edge" />',
	'  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />',
	'  <meta name="description" content="" />',
	'  <meta name="author" content="" />',
	'  <title>'+ config.html.title +'</title>',
	'  <link href="favicon.ico" rel="icon" />',
	'  <link href="css/bootstrap.min.css" rel="stylesheet" />',
	'  <link href="css/main.css" rel="stylesheet" />',
	'  <body>'
]

page.body = ( htmlFile ) => {
	var filePath = './assets/' + htmlFile
	var htmlCode = '\n'
	let i = 0
	var raw = fs.readFileSync( filePath, 'utf8' ).toString().split('\n')
	for ( i in raw ) {
		htmlCode += '    ' + raw[i] + '\n'
	}
	return htmlCode
}

page.end = [
	'    <script src="js/jquery.min.js"></script>',
 	'    <script src="js/bootstrap.min.js"></script>',
	'    <script src="socket.io/socket.io.js"></script>',
	'    <script src="js/main.js"></script>',
	'  </body>',
	'</html>'
]


// render complete site //
module.exports.render = () => {
	var html = ''
	let i = 0
	for ( i in page.start ) {
		html += page.start[i] + '\n'
	}
	html += page.body( config.html.body )
	i = 0
	for ( i in page.end ) {
		html += page.end[i] + '\n'
	}
	return html
}

# **[ README ]** #

** WIP.** This version is partial tested. Running this, is on your own risk.

# **[ LATEST NEWS ]** #

Translated everything from Danish to English, and made the project public.
The project is also being prepared to run with other robot projects, like our own Beta-Bot.

# **[ CONCEPT ]** #

I wanted to a simple rover / robot with 2 wheels, that could do the standard things like Line following, obstacle avoidance, and remote control.
And I wanted to make it with NodeJS, and without a lot of middleware.
I hope to add more features to the robot, and make it more streamline.

# **[ INFO ]** #

The base MCU is right now a Raspberry Pi 2/3 but the rest of the hardware runs from the SPI port, so it would usable on other MCUs.
This code is running in NodeJS that runs nicely on the PIs, and is really flexible.

Parts:
- Raspberry Pi 2/3 : With the rPI3 we don't need a WiFi card.
- Proto HAT : A prototype HAT from ebay.
- Raspberry Pi Camera : For realtime video stream.
- MCP23S27 : A GPIO port expander, with 16 digital I/O ports.
- MCP3008 : A ADC with 8 analog inputs.
- H-Bridge : A L298 based h-bridge from ebay.
